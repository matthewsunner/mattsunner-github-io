// Global Variables
const fact = document.querySelector('#fact');
const factText = document.querySelector('#factText'); 
const numberView = document.querySelector('#number-view');
const randomView = document.querySelector('#random-view');
const numberInput = document.querySelector('#numberInput');
const randomFactInput = document.querySelector('#btnInput')
const randomFactText = document.querySelector('#factText'); 


// Event Listeners
numberInput.addEventListener('input', getFactFetchNumber);
randomFactInput.addEventListener('click', getFactFetchRandom);


// FETCH API CALL Number Trivia

function getFactFetchNumber() {
    const number = numberInput.value; 

    fetch('http://numbersapi.com/'+number)
        .then(response => response.text())
        .then(data => {
            if(number != ''){
                fact.style.display = 'block';
                factText.innerText = data;
            }
        })
        .catch(err => console.log(err));
}

// FETCH API CALL Random Trivia

function getFactFetchRandom() {
    const url = 'http://randomuselessfact.appspot.com/random.json?language=en';
    const proxy = 'https://cors-anywhere.herokuapp.com/'; 

    fetch(proxy + url) 
        .then(response => response.json())
        .then(data => {
            randomFactText.style.display = 'block';
            randomFactInput.innerText = data.text; 
        })
        .catch(err => console.log(err));
}


// Generate Cards to view the application
function showNumberFact() {
    numberView.style.display = "block";
}

function showRandomFact() {
    randomView.style.display = "block";
}
